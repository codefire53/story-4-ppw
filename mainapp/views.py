from django.shortcuts import render

# Create your views here.
def about(request):
    return render(request,'about2.html')
def skills(request):
    return render(request,'skill2.html')
def projects(request):
    return render(request,'project2.html')
def experiences(request):
    return render(request,'experience2.html')
def education(request):
    return render(request,'education2.html')
def contact(request):
    return render(request,'contact2.html')
def landing(request):
    return render(request,'landing.html')